﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IDesign.iFX.Utilities
{
    [DataContract]
    public class Response<TR>
    {
        [DataMember]
        public TR Data { get; internal set; }

        [DataMember]
        public IEnumerable<Error> Errors { get; internal set; }

        public Response(TR data, IEnumerable<Error> errors)
        {
            Data = data;
            Errors = errors;
        }

        public Response(IEnumerable<Error> errors)
        {
            Errors = errors;
        }

        public Response(TR data)
        {
            Data = data;
            Errors = new Error[0];
        }
    }
}