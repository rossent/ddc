﻿using System.Runtime.Serialization;

namespace IDesign.iFX.Utilities
{
    [DataContract]
    public class Error
    {
        [DataMember]
        public string Description { get; internal set; }

        public Error(string description)
        {

            Description = description;
        }

        public override string ToString()
        {
            return Description;
        }
    }
}