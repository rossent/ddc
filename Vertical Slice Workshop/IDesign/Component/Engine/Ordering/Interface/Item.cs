﻿using System;
using System.Runtime.Serialization;

namespace IDesign.Engine.Ordering.Interface
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}