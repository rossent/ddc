﻿using System.Runtime.Serialization;

namespace IDesign.Engine.Ordering.Interface
{
    [DataContract]
    public class ItemCriteria
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Location { get; set; }
    }
}