﻿using System.Threading.Tasks;
using IDesign.Access.Customer.Interface;
using IDesign.Access.Menu.Interface;
using ServiceModelEx.ServiceFabric;

using IDesign.iFX.Service;
using MethodModelEx.Microservices;

using IDesign.Engine.Ordering.Interface;

#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;
#else
using System.Fabric;
#endif

namespace IDesign.Engine.Ordering.Service
{
   [ApplicationManifest("IDesign.Microservice.Sales","OrderingEngine")]
   public class OrderingEngine : ServiceBase, IOrderingEngine
   {
      public OrderingEngine(StatelessServiceContext context) : base(context)
      {}

      /// <inheritdoc />
      async Task<MatchResponse> IOrderingEngine.MatchAsync(ItemCriteria criteria)
      {
          ICustomerAccess customerAccessProxy = Proxy.ForComponent<ICustomerAccess>(this);
          await customerAccessProxy.FilterAsync(new FilterRequest());

          IMenuAccess menuAccessProxy = Proxy.ForComponent<IMenuAccess>(this);
          await menuAccessProxy.FilterAsync(new MenuFilter());

          return new MatchResponse();
      }
   }
}
