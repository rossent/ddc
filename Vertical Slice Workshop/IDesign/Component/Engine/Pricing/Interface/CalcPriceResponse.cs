﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IDesign.Engine.Pricing.Interface
{
    [DataContract]
    public class CalcPriceResponse
    {
        // Need to know what's required to calc a price
        [DataMember]
        public decimal Price { get; set; }
    }
}
