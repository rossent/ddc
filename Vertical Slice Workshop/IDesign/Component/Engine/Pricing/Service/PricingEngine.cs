﻿using System.Threading.Tasks;
using IDesign.Access.Specials.Interface;
using ServiceModelEx.ServiceFabric;

using IDesign.iFX.Service;
using MethodModelEx.Microservices;

using IDesign.Engine.Pricing.Interface;

#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;
#else
using System.Fabric;
#endif

namespace IDesign.Engine.Pricing.Service
{
   [ApplicationManifest("IDesign.Microservice.Sales","PricingEngine")]
   public class PricingEngine : ServiceBase, IPricingEngine
   {
      public PricingEngine(StatelessServiceContext context) : base(context)
      {}

      /// <inheritdoc />
      async Task<CalcPriceResponse> IPricingEngine.CalcPriceAsync(CalcPriceParameters parameters)
      {

          var criteria = new SpecialsCriteria();
          //TODO: fill the criteria
          ISpecialsAccess specialsProxy = Proxy.ForComponent<ISpecialsAccess>(this);
          await specialsProxy.FilterAsync(criteria);

          return new CalcPriceResponse();
      }
   }
}
