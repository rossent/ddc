﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IDesign.Engine.Validation.Interface
{
    [DataContract]
    public class ValidateResponse
    {
        [DataMember]
        public bool Valid { get; set; }
    }
}
