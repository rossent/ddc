﻿using System.ServiceModel;
using System.Threading.Tasks;
using IDesign.iFX.Utilities;
using ServiceModelEx.ServiceFabric.Services.Remoting;

namespace IDesign.Access.Menu.Interface
{
   [ServiceContract]
   public interface IMenuAccess : IService
   {
      [OperationContract]
      Task<Response<MenuResponse>> FilterAsync(MenuFilter menuFilter);
      [OperationContract]
      Task StoreAsync();
   }
}
