﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using ServiceModelEx.ServiceFabric;

using IDesign.iFX.Service;
using MethodModelEx.Microservices;

using IDesign.Access.Restaurant.Interface;

#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;
#else
using System.Fabric;
#endif

namespace IDesign.Access.Restaurant.Service
{
   [ApplicationManifest("IDesign.Microservice.Sales","RestaurantAccess")]
   public class RestaurantAccess : ServiceBase, IRestaurantAccess
   {
      public RestaurantAccess(StatelessServiceContext context) : base(context)
      {}

      
      /// <inheritdoc />
      async Task<IEnumerable<Interface.Restaurant>> IRestaurantAccess.FilterAsync(RestaurantCriteria criteria)
      {
          return new Interface.Restaurant[0];
      }

      /// <inheritdoc />
      async Task<StoreRestaurantResponse> IRestaurantAccess.StoreAsync(Interface.Restaurant restaurant)
      {
          return new StoreRestaurantResponse()
          {
              Restaurant = restaurant
          };
      }
   }
}
