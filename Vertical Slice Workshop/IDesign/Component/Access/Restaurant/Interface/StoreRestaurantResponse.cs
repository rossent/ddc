﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using IDesign.iFX.Utilities;

namespace IDesign.Access.Restaurant.Interface
{
    [DataContract]
    public class StoreRestaurantResponse
    {
        [DataMember]
        public IEnumerable<Error> Errors { get; set; }
        [DataMember]
        public Restaurant Restaurant { get; set; }
    }
}