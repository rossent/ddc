﻿using System.Runtime.Serialization;

namespace IDesign.Access.Restaurant.Interface
{
    [DataContract]
    public class RestaurantCriteria
    {
        [DataMember]
        public string Location { get; set; }
    }
}