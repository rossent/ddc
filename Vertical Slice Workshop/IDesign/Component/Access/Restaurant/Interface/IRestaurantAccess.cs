﻿using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

using ServiceModelEx.ServiceFabric.Services.Remoting;

namespace IDesign.Access.Restaurant.Interface
{
   [ServiceContract]
   public interface IRestaurantAccess : IService
   {
      [OperationContract]
      Task<IEnumerable<Restaurant>> FilterAsync(RestaurantCriteria criteria);
      [OperationContract]
      Task<StoreRestaurantResponse> StoreAsync(Restaurant restaurant);
   }
}
