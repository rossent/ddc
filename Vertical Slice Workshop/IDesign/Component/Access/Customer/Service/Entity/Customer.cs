﻿using IDesign.Framework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDesign.Access.Customer.Service.Entity
{
    [Table]
    public class Customer : TableBase
    {
        [Column]
        public string Name
        { get; set; }
    }
}
