﻿using System.Threading.Tasks;
using ServiceModelEx.ServiceFabric;
using IDesign.iFX.Service;
using MethodModelEx.Microservices;
using IDesign.Access.Customer.Interface;
using IDesign.iFX.Utilities;
using System.Collections.Generic;
using IDesign.Framework.Repository;
#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;

#else
using System.Fabric;
#endif

namespace IDesign.Access.Customer.Service
{
    [ApplicationManifest("IDesign.Microservice.Sales", "CustomerAccess")]
    public class CustomerAccess : ServiceBase, ICustomerAccess
    {
        public CustomerAccess(StatelessServiceContext context) : base(context)
        {
        }

        public async Task<Response<CustomerResponse>> FilterAsync(FilterRequest filterRequest)
        {
            Entity.ICustomerRepository customerRepository = RepositoryFactory.Create<Entity.ICustomerRepository>();
            IEnumerable<Entity.Customer> entityCustomer = customerRepository.Get<Entity.Customer>(filterRequest.NameCriteria);
            return MapEntityToDTO(entityCustomer);
        }

        //IEnumerable<Interface.Customer> MapEntityToDTO(IEnumerable<Entity.Customer> customer)
        Response<CustomerResponse> MapEntityToDTO(IEnumerable<Entity.Customer> customer)
        {
            return new Response<CustomerResponse>(new CustomerResponse());
        }

        async Task ICustomerAccess.StoreAsync()
        {
            //TODO: here we need to map from the DTO to the entity and save it in the DB
        }
    }
}