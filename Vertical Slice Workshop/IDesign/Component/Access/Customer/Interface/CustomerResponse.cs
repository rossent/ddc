﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IDesign.Access.Customer.Interface
{
    
    [DataContract]
    public class CustomerResponse
    {
        [DataMember]
        public IEnumerable<Customer> Customers  { get; set; }
    }
}