﻿using System.Runtime.Serialization;

namespace IDesign.Access.Customer.Interface
{
    [DataContract]
    public class FilterRequest
    {
        public string NameCriteria { get; set;}

    }
}