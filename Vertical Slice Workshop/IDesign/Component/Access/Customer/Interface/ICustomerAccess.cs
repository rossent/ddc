﻿using System.ServiceModel;
using System.Threading.Tasks;
using IDesign.iFX.Utilities;
using ServiceModelEx.ServiceFabric.Services.Remoting;

namespace IDesign.Access.Customer.Interface
{
   [ServiceContract]
   public interface ICustomerAccess : IService
   {
      [OperationContract]
      Task<Response<CustomerResponse>> FilterAsync(FilterRequest filterRequest);
      [OperationContract]
      Task StoreAsync();
   }
}
