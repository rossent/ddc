﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceModelEx.ServiceFabric;

using IDesign.iFX.Service;
using MethodModelEx.Microservices;

using IDesign.Access.Specials.Interface;
using IDesign.iFX.Utilities;
#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;
#else
using System.Fabric;
#endif

namespace IDesign.Access.Specials.Service
{
   [ApplicationManifest("IDesign.Microservice.Sales","SpecialsAccess")]
   public class SpecialsAccess : ServiceBase, ISpecialsAccess
   {
      public SpecialsAccess(StatelessServiceContext context) : base(context)
      {}

      /// <inheritdoc />
      async Task<IEnumerable<Interface.Specials>> ISpecialsAccess.FilterAsync(SpecialsCriteria criteria)
      {
          return new Interface.Specials[0];
      }

      async Task<Response<Interface.Specials>> ISpecialsAccess.StoreAsync(Interface.Specials specials)
      {
          return new Response<Interface.Specials>(specials);
      }
   }
}
