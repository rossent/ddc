﻿using System;
using System.Runtime.Serialization;

namespace IDesign.Access.Specials.Interface
{
    [DataContract]
    public class Specials
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }
}