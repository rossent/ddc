﻿using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using IDesign.iFX.Utilities;
using ServiceModelEx;
using ServiceModelEx.ServiceFabric.Services.Remoting;

namespace IDesign.Access.Specials.Interface
{
   [ServiceContract]
   public interface ISpecialsAccess : IService
   {
      [OperationContract]
      Task<IEnumerable<Specials>> FilterAsync(SpecialsCriteria criteria);

      [OperationContract]
      Task<Response<Specials>> StoreAsync(Specials specials);
   }
}
