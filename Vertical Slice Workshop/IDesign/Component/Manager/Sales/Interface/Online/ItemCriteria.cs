﻿using System;
using System.Runtime.Serialization;

namespace IDesign.Manager.Sales.Interface.Online
{
    [DataContract]
    public class ItemCriteria
    {
        [DataMember]
        public string Name { get; set; }
    }
}