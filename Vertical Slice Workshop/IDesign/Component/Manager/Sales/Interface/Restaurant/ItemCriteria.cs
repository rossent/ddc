﻿using System;
using System.Runtime.Serialization;

namespace IDesign.Manager.Sales.Interface.Restaurant
{
    [DataContract]
    public class ItemCriteria
    {
        [DataMember]
        public string Name { get; set; }
    }
}