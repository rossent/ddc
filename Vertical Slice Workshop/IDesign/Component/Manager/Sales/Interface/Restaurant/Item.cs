﻿using System;
using System.Runtime.Serialization;

namespace IDesign.Manager.Sales.Interface.Restaurant
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}