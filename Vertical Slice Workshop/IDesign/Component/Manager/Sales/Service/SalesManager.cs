﻿using System.Runtime.InteropServices;
using System.Threading.Tasks;
using ServiceModelEx.ServiceFabric;
using IDesign.iFX.Service;
using MethodModelEx.Microservices;
using IDesign.Access.Restaurant.Interface;
using IDesign.Engine.Validation.Interface;
using IDesign.Engine.Pricing.Interface;
using Online = IDesign.Manager.Sales.Interface.Online;
using Restaurant = IDesign.Manager.Sales.Interface.Restaurant;
using Internal = IDesign.Engine.Ordering.Interface;


#if ServiceModelEx_ServiceFabric
using ServiceModelEx.Fabric;
#else
using System.Fabric;
#endif

namespace IDesign.Manager.Sales.Service
{
    [ApplicationManifest("IDesign.Microservice.Sales", "SalesManager")]
    public class SalesManager : ServiceBase, Online.ISalesManager, Restaurant.ISalesManager
    {
        public SalesManager(StatelessServiceContext context) : base(context)
        {
            //test2
        }

        Internal.ItemCriteria MapPublicToInternal(Online.ItemCriteria criteria)
        {
            return new Internal.ItemCriteria();
        }
        Online.Item MapInternalToPublic(Internal.Item item)
        {
            return new Online.Item();
        }

        async Task<Online.Item> Online.ISalesManager.FindItemAsync(Online.ItemCriteria criteria)
        {
            var validateCriteria = new ValidateCriteria(); // should be using a proxy

            IValidationEngine validationProxy = Proxy.ForComponent<IValidationEngine>(this);
            var validateResponse = await validationProxy.ValidateAsync(validateCriteria);

            var restCriteria = new RestaurantCriteria()
            {
                //TODO: get the location from the item criteria
                Location = ""
            };
            IRestaurantAccess restaurantProxy = Proxy.ForComponent<IRestaurantAccess>(this);
            var restaurants = await restaurantProxy.FilterAsync(restCriteria);
            
            Internal.IOrderingEngine orderingProxy = Proxy.ForComponent<Internal.IOrderingEngine>(this);
            var matchResponse = await orderingProxy.MatchAsync(MapPublicToInternal(criteria));

            var calcPriceParameters = new CalcPriceParameters(); // should be using a proxy

            IPricingEngine pricingProxy = Proxy.ForComponent<IPricingEngine>(this);
            var calcPriceReponse = await pricingProxy.CalcPriceAsync(calcPriceParameters);
            
            //TODO: get the item from the match response and update the price
            Internal.Item item = new Internal.Item();

            return MapInternalToPublic(item);
            
        }

        async Task Restaurant.ISalesManager.FindItemAsync(Restaurant.ItemCriteria criteria)
        {
        }
    }
}