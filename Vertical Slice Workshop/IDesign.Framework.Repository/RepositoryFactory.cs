﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace IDesign.Framework.Repository
{
    public static class RepositoryFactory
    {
        static Type Resolve<I>() where I : class
        {
            string typeName = string.Empty;

            string repositoryName = typeof(I).Name.Replace("I", "");
            typeName = typeof(I).Namespace + "." + repositoryName;

            Type implementationType = typeof(I).Assembly.GetType(typeName);
            Debug.Assert(implementationType != null, "You did not follow the rules...");

            return implementationType;
        }
        public static I Create<I>() where I : class, IRepository
        {
            return Activator.CreateInstance(Resolve<I>()) as I;
        }
    }
}
