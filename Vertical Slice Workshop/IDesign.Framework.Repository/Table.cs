﻿using System;

namespace IDesign.Framework.Repository
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    { }
}
