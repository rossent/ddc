﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDesign.Framework.Repository
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Column : Attribute
    { }
}
