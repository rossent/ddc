﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDesign.Framework.Repository
{
    public interface IRepository
    {
        IEnumerable<T> Get<T>(string key) where T : TableBase, new();
    }
}
